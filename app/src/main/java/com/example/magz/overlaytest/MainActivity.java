package com.example.magz.overlaytest;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private View santa_hat;
    private View imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);
        santa_hat = findViewById(R.id.santa_hat);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        int[] location_img = new int[2];
        int[] location_santa = new int[2];
        imageView.getLocationInWindow(location_img);
        System.out.println(Arrays.toString(location_img));

        santa_hat.getLocationInWindow(location_santa);
        System.out.println(Arrays.toString(location_santa));

        santa_hat.setX(location_img[0] - location_santa[0]);
        santa_hat.setY(location_img[1] - location_santa[1] - convertDpToPixel(27.5f,this));

        santa_hat.getLocationInWindow(location_santa);
        System.out.println(Arrays.toString(location_santa));
    }

    private static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }
}
